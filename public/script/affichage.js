// JavaScript Document
"use strict";

//  ======================================
//              ACTUS
//  ======================================

// Créer une vignette avec une date au format "01-JAN"
function vignetteDate(elemTr, date, afficheAnnee) {
    var months = ["jan", "f&eacute;v", "mar", "avr", "mai", "jun", "juil", "ao&ucirc;", "sep", "oct", "nov", "d&eacute;c"],
        d = new Date(date),
        jour = d.getDate().toString(),
        mois = months[d.getMonth()],
		annee = d.getFullYear(),
        elemTd,
        elemDiv;
	
	elemTd = document.createElement("td");
    elemTr.appendChild(elemTd);
	elemDiv = document.createElement("div");
	elemTd.appendChild(elemDiv);
    elemDiv.setAttribute("class", "vignette_date");
    
    elemDiv.innerHTML = '<span class="jour">' + jour + '</span><br /><span class="mois">' + mois + (afficheAnnee ? ' ' + annee : '') + '</span>';
    
}


// Créer 1 ligne de table avec 1 ou 2 vignette selon le besoin, les infos et les boutons de téléchargement
function afficheActu(elemTable, actu) {
	var elemTr, elemTd, contenu, elemA, elemImg, elemH2;
    
    // Créer la ligne de tableau
    elemTr = document.createElement("tr");
    elemTable.appendChild(elemTr);
    
    // Dates
    vignetteDate(elemTr, actu.dbt, true);
    
    // Titre
    elemTd = document.createElement("td");
    elemTr.appendChild(elemTd);
    elemTd.setAttribute("class", "info_stage");
    elemH2 = document.createElement("h2");
    contenu = document.createTextNode(actu.titre);
    elemH2.appendChild(contenu);
    elemTd.appendChild(elemH2);
	
	
	// Infos
    contenu = document.createTextNode(actu.info);
    elemTd.appendChild(contenu);
    
    // Si le contenu comporte un lien
    if (actu.lien.length > 0) {
        // Créer le lien
        elemA = document.createElement("a");
        elemA.setAttribute("href", actu.lien);
        contenu = document.createTextNode(" Lien >>");
        elemA.appendChild(contenu);
        
        // Ajouter le lien
        elemTd.appendChild(elemA);
    }
    
    
    
	
	// Boutons
	
	// Créer la cellule, dans tous les cas
	elemTd = document.createElement("td");
	elemTr.appendChild(elemTd);
    
    // Créer l'image	
	if (actu.img.length > 0)
	{
		// Créer le lien
		elemA = document.createElement("a");
		elemTd.appendChild(elemA);
		elemA.setAttribute("href", "img/actu/"+actu.img);
	
		elemImg = document.createElement("img");
		elemA.appendChild(elemImg);
		elemImg.setAttribute("src", "img/actu/"+actu.img);
		elemImg.setAttribute("alt", actu.img);
		elemImg.setAttribute("title", actu.titre);
	}
    
}


// Remplir la liste des actu jusqu'au nombre maximum indiqué
function remplirActus(nbMax)
{
    var actu, i, elemUL, elemListe, elemTable, taille;
    
    if (listeActu == null){
        console.log("fichier actu.js non chargé");
        return;
    }
    taille = listeActu.actus.length;
	
	
	// Trouver la liste
    elemUL = document.getElementById("liste");
    console.log("ok");
    if (elemUL == null)
    {
        console.log("Element liste non trouve");
        return;
    }
    
    // Effacer tous les éléments
    while (elemUL.hasChildNodes())
    {
        elemUL.removeChild(elemUL.firstChild);
        console.log("Element de liste supprime");
    }
							
	// Ajouter chaque actu
	for ( i=0; i < taille && i < nbMax ; i++)
	{
		actu = listeActu.actus[i];
        

        // Ajouter l'élément, tout en gardant les éléments précédents
        // Créer l'élément de liste
        elemListe = document.createElement("li");
        elemUL.appendChild(elemListe);

        // Créer la table
        elemTable = document.createElement("table");
        elemListe.appendChild(elemTable);

        console.log("Ajout de l'actu "+actu.dbt.toString());
        afficheActu(elemTable, actu);
        //$("#liste").fadeOut();

        // Afficher progressivement la ligne
        $("ul#liste li").fadeIn(i*300);


    }

}

//  ==================================================================
//                    P R O F S
//  ==================================================================


// Créer 1 ligne de table avec 1 ou 2 vignette selon le besoin, les infos et les boutons de téléchargement
function afficheProf(elemTable, prof) {
	var elemTr, elemTd, contenu, elemImg, elemH2, elemH3, elemA;
    
    // Créer la ligne de tableau
    elemTr = document.createElement("tr");
    elemTable.appendChild(elemTr);
	
	
	// Nom
    elemTd = document.createElement("td");
    elemTr.appendChild(elemTd);
    elemTd.setAttribute("class", "info_stage");
    elemH2 = document.createElement("h2");
    contenu = document.createTextNode(prof.nom);
    elemH2.appendChild(contenu);
    elemTd.appendChild(elemH2);
    
    // Grade
    elemH3 = document.createElement("h3");
    contenu = document.createTextNode(prof.grade);
    elemH3.appendChild(contenu);
    elemTd.appendChild(elemH3);
    
    // Bio
    contenu = document.createTextNode(prof.bio);
    elemTd.appendChild(contenu);
        
    
	
	// Boutons
	
	// Créer la cellule, dans tous les cas
	elemTd = document.createElement("td");
	elemTr.appendChild(elemTd);
    
    // Créer l'image	
	if (prof.img.length > 0)
	{
		// Créer le lien
		elemA = document.createElement("a");
		elemTd.appendChild(elemA);
		elemA.setAttribute("href", "img/prof/"+prof.img);
	
		elemImg = document.createElement("img");
		elemA.appendChild(elemImg);
		elemImg.setAttribute("src", "img/prof/"+prof.img);
		elemImg.setAttribute("alt", prof.nom);
		elemImg.setAttribute("title", prof.nom);
	}
    
}


// Remplir la liste des prof
function remplirProfs()
{
    var prof, i, elemUL, elemListe, elemTable, taille;
    
    if (listeProfs == null){
        console.log("fichier prof.js non chargé");
        return;
    }
    taille = listeProfs.profs.length;
	
	
	// Trouver la liste
    elemUL = document.getElementById("liste");
    console.log("ok");
    if (elemUL == null)
    {
        console.log("Element liste non trouve");
        return;
    }
    
    // Effacer tous les éléments
    while (elemUL.hasChildNodes())
    {
        elemUL.removeChild(elemUL.firstChild);
        console.log("Element de liste supprime");
    }
							
	// Ajouter chaque actu
	for ( i=0; i < taille ; i++)
	{
		prof = listeProfs.profs[i];
        

        // Ajouter l'élément, tout en gardant les éléments précédents
        // Créer l'élément de liste
        elemListe = document.createElement("li");
        elemUL.appendChild(elemListe);

        // Créer la table
        elemTable = document.createElement("table");
        elemListe.appendChild(elemTable);

        console.log("Ajout du prof "+prof.nom);
        afficheProf(elemTable, prof);
        //$("#liste").fadeOut();

        // Afficher progressivement la ligne
        $("ul#liste li").fadeIn(i*300);


    }

}

function resumerProfs()
{
    var prof, i, elemH2, contenu, taille;
    
    if (listeProfs == null){
        console.log("fichier prof.js non chargé");
        return;
    }
    taille = listeProfs.profs.length;
	
	
	// Trouver la liste
    elemH2 = document.getElementById("profs");
    console.log("ok");
    if (elemH2 == null)
    {
        console.log("Element H2 profs non trouve");
        return;
    }
    
    // Effacer tous les éléments
    while (elemH2.hasChildNodes())
    {
        elemH2.removeChild(elemH2.firstChild);
        console.log("Element de liste supprime");
    }
							
	// Ajouter chaque prof
	for ( i=0; i < taille ; i++)
	{
		prof = listeProfs.profs[i];
        

        // Ajouter l'élément, tout en gardant les éléments précédents
        // Créer l'élément de liste
        console.log("Ajout du prof "+prof.nom);
        contenu = document.createTextNode(prof.nom + " - " + prof.grade);
        elemH2.appendChild(contenu);
        elemH2.appendChild(document.createElement("br"));

    }

}


//  ==================================================================
//                    D O C S
//  ==================================================================


// Créer 1 ligne de table avec 1 ou 2 vignette selon le besoin, les infos et les boutons de téléchargement
function afficheDoc(elemTable, doc) {
	var elemTr, elemTd, contenu, elemImg, elemH2, elemH3, elemA;
    
    // Créer la ligne de tableau
    elemTr = document.createElement("tr");
    elemTable.appendChild(elemTr);
	
	
	// Nom
    elemTd = document.createElement("td");
    elemTr.appendChild(elemTd);
    elemTd.setAttribute("class", "info_stage");
    elemH2 = document.createElement("h2");
    contenu = document.createTextNode(doc.titre);
    elemH2.appendChild(contenu);
    elemTd.appendChild(elemH2);

    
    // info
    contenu = document.createTextNode(doc.info);
    elemTd.appendChild(contenu);
        
    
	
	// Boutons
    // Créer la cellule, dans tous les cas
	elemTd = document.createElement("td");
	elemTr.appendChild(elemTd);
    
    // Créer l'image
	if (doc.lien.length > 0)
	{
		// Créer le lien
		elemA = document.createElement("a");
		elemTd.appendChild(elemA);
		elemA.setAttribute("href", doc.lien);
        elemA.setAttribute("target", "_blank");
	
		elemImg = document.createElement("img");
		elemA.appendChild(elemImg);
		elemImg.setAttribute("src", "img/pdf-icon.png");
		elemImg.setAttribute("alt", doc.lien);
		elemImg.setAttribute("title", doc.lien);
	}
    
}


// Remplir la liste des documents
function remplirDocs()
{
    var doc, i, elemUL, elemListe, elemTable, taille;
    
    if (listeDocs == null){
        console.log("fichier docs.js non chargé");
        return;
    }
    taille = listeDocs.docs.length;
	
	
	// Trouver la liste
    elemUL = document.getElementById("liste");
    console.log("ok");
    if (elemUL == null)
    {
        console.log("Element liste non trouve");
        return;
    }
    
    // Effacer tous les éléments
    while (elemUL.hasChildNodes())
    {
        elemUL.removeChild(elemUL.firstChild);
        console.log("Element de liste supprime");
    }
							
	// Ajouter chaque actu
	for ( i=0; i < taille ; i++)
	{
		doc = listeDocs.docs[i];
        

        // Ajouter l'élément, tout en gardant les éléments précédents
        // Créer l'élément de liste
        elemListe = document.createElement("li");
        elemUL.appendChild(elemListe);

        // Créer la table
        elemTable = document.createElement("table");
        elemListe.appendChild(elemTable);

        console.log("Ajout du doc "+doc.titre);
        afficheDoc(elemTable, doc);
        //$("#liste").fadeOut();

        // Afficher progressivement la ligne
        $("ul#liste li").fadeIn(i*300);


    }

}

// ===================================================
//              C O N T A C T S
// ===================================================


// Créer 1 ligne de table avec 1 ou 2 vignette selon le besoin, les infos et les boutons de téléchargement
function afficheContact(elemTable, contact) {
	var elemTr, elemTd, contenu, elemH3;
    
    // Créer la ligne de tableau
    elemTr = document.createElement("tr");
    elemTable.appendChild(elemTr);
	
	
	// Nom
    elemTd = document.createElement("td");
    elemTr.appendChild(elemTd);
    elemTd.setAttribute("class", "info_stage");
    elemH3 = document.createElement("h3");
    contenu = document.createTextNode(contact.nom);
    elemH3.appendChild(contenu);
    elemTd.appendChild(elemH3);

    
    // fonction
    contenu = document.createTextNode(contact.fct);
    elemTd.appendChild(contenu);
        
    
	
	// Boutons
    // Créer la cellule, dans tous les cas
	elemTd = document.createElement("td");
	elemTr.appendChild(elemTd);
    
    // téléphone
    contenu = document.createTextNode(contact.tel);
    elemTd.appendChild(contenu);
    
}


// Remplir la liste des contacts
function remplirContacts()
{
    var contact, i, elemUL, elemListe, elemTable, taille;
    
    if (listeContact == null){
        console.log("fichier contact.js non chargé");
        return;
    }
    taille = listeContact.contact.length;
	
	
	// Trouver la liste
    elemUL = document.getElementById("liste");
    console.log("ok");
    if (elemUL == null)
    {
        console.log("Element liste non trouve");
        return;
    }
    
    // Effacer tous les éléments
    while (elemUL.hasChildNodes())
    {
        elemUL.removeChild(elemUL.firstChild);
        console.log("Element de liste supprime");
    }
							
	// Ajouter chaque actu
	for ( i=0; i < taille ; i++)
	{
		contact = listeContact.contact[i];
        

        // Ajouter l'élément, tout en gardant les éléments précédents
        // Créer l'élément de liste
        elemListe = document.createElement("li");
        elemUL.appendChild(elemListe);

        // Créer la table
        elemTable = document.createElement("table");
        elemListe.appendChild(elemTable);

        console.log("Ajout du contact "+contact.nom);
        afficheContact(elemTable, contact);
        //$("#liste").fadeOut();

        // Afficher progressivement la ligne
        $("ul#liste li").fadeIn(i*300);


    }

}

