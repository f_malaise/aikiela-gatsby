// Structure de chaque actu : 
//	{dbt: "obligtoire AAAA-MM-JJ", titre: "Rentrée...", info: "blablabla...", 
//  img: "nom du fichier dans img/actu/", lien: "lien href facultatif" }
var listeActu = { actus:
    [
        {dbt: "2022-08-29", titre: "Rentrée saison 2022-2023", info: "Ça y est ! C'est la reprise à l'École Lauragaise d'Aïkido. Si vous voulez faire un cours d'essai, il vous suffit de venir un soir (voir les horaires) au dojo de Toulouse ou Caraman (toutes les infos sur la page de contact). Pas besoin de kimono pour tester, un jogging, un T-shirt suffisent. On vous attend !", img: "rentree2021.png", lien: "contact.html"},
        {dbt: "2022-07-23", titre: "Stage international d’aïkido jeunes organisé par l’IAF à Papendal (Hollande)", info: "Dans le cadre du stage international d’aïkido jeunes organisé par l’IAF à Papendal (Hollande) ce week-end des 23 et 24 juillet , la FFAAA est fière d’envoyer une délégation d’une dizaine de jeunes pratiquante-s de 11 à 18 ans. Pierre Marcon et les jeunes du club étaient du voyage !", img: "Papendal-2022.jpg", lien: "https://aikido.com.fr/uncategorized/stage-international-daikido-jeunes-organise-par-liaf-a-papendal-hollande/"}
    ]
    };