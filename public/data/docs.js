// Structure de chaque actu : 
//	{ titre: "Rentrée...", info: "blablabla...", lien: "lien href facultatif" }
var listeDocs = { docs:
    [
       /* {titre: "Dossier d'inscription complet", info: "Contenant la fiche d'inscription pour le club de Toulouse ou Caraman, l'autorisation parentale et le certificat médical ou le questionnaire de santé pour la demande ou le renouvellement de licence. Vous pouvez complèter votre dossier en version numérique ou en version papier (c'est donc à vous d’imprimer les pages nécessaires). La version numérique est à renvoyé à ela.aikido@gmail.com", lien: "contact.html"},*/
        {titre: "Note d’informations", info: "Concernant le certificat médical", lien: "https://aikido.com.fr/docs/note-dinfo-certificat-medical/"},/*
        {titre: "Demande passeport", info: "Fiche de demande de passeport d'aïkido pour les passages de grade et les stages", lien: "https://aikido.com.fr/docs/note-dinfo-certificat-medical/"},*/
        {titre: "Guide du débutant", info: "Philosophie de l'aïkido et de notre pratique, liste de techniques et grades", lien: "docs/aikido-ffaaa-guide-debutant.pdf"}/*,
        {titre: "Candidature au passage de grade", info: "Dossier à remplir pour s'inscrire à un passage de grade", lien: "https://aikido.com.fr/docs/note-dinfo-certificat-medical/"}*/
    ]
    };